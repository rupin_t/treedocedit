<?php include('core.php'); ?>

<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Tree Document Edit Interface</title>
        <?php include('dependencies.html'); ?>
    </head>
    <body>
    	<?php
  			if (!$_SESSION['logged'])
  				include('login.html');
			else {
				print '<input type="hidden" id="user-id" value="'. $_SESSION['user_id'] .'">';
	   			include('edit.html');
			}
    	?>
    </body>
</html>x