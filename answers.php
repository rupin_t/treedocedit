<?php
    include('core.php');
	
	$response = '{"error":"nothing done", "errno":-1}';
	if ($_SESSION['logged']) {
        if (isset($_GET['query'])) {
    		if ($_GET['query'] == 'list') {
    			$query = 'SELECT tbl_answers.* FROM tbl_answers JOIN tbl_documents ON tbl_documents.id = tbl_answers.document_id
    									 WHERE tbl_documents.owner_id = ?';
    			$args = array($_SESSION['user_id']);
    			if (isset($_GET['document_id'])) {
    				$query .=  ' AND tbl_documents.document_id = ?';
    				array_push($args, $_GET['document_id']);
    			}
    			if (isset($_GET['greaterThan'])) {
    				$query .= ' AND tbl_answers.id > ?';
    				array_push($args, $_GET['greaterThan']);
    			}
    			$sth = $dbh->prepare($query);
    			$sth->execute($args);
    			$response = json_encode(array('data'=>$sth->fetchAll(PDO::FETCH_ASSOC)));
    		}
        }
		if (isset($_POST['action'])) {
			if ($_POST['action'] == 'insert' && isset($_POST['data'])) {
				$data = json_decode(stripslashes($_POST['data']), true);
			
				foreach ($data as &$value) {
					$sth = $dbh->prepare('INSERT INTO tbl_answers (document_id, user_id, node_id, response) VALUES (?, ?, ?, ?)');
					$sth->execute(array($value['document_id'], $_SESSION['user_id'], $value['node_id'], $value['value']));
					
					$sth = $dbh->prepare('SELECT id FROM tbl_answers WHERE user_id = ? ORDER BY id DESC LIMIT 1');
					$sth->execute(array($_SESSION['user_id']));
					$d = $sth->fetchAll();
					if (count($d) == 1)
						$value = $d[0]['id'];
					else $value = -1;
				}

				$response = json_encode(array('data'=>$data));
			}
		}
	}
	
	print $response;
?>