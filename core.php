<?php
	session_start();
   
    function strip_magic_quotes($arr) {
        foreach($arr as $k => $v) {
            if (is_array($v)) {
                $arr[$k] = strip_magic_quotes($v);
            }
            else {
                $arr[$k] = stripslashes($v);
            }
        }
        return $arr;
    }
    if (get_magic_quotes_gpc()) {
        if (!empty($_GET)) {
            $_GET = strip_magic_quotes($_GET);
        }
        if (!empty($_POST)) {
            $_POST = strip_magic_quotes($_POST);
        }
        if (!empty($_COOKIE)) {
            $_COOKIE = strip_magic_quotes($_COOKIE);
        }
    }

	try {
		$dbh = new PDO('mysql:host=localhost;port=8889;dbname=treedoc', 'root', 'root');

        $sth = $dbh->prepare('SELECT public_key FROM tbl_server_infos LIMIT 1');
        $sth->execute();
        $res = $sth->fetchAll();

        if (count($res) == 1) {
            $_SESSION['server_key'] = $res[0]['public_key'];
        }
        else {
            list($usec, $sec) = explode(' ', microtime());
            srand((float) $sec + ((float) $usec * 100000));
            $_SESSION['server_key'] = sha1(rand());
            $sth = $dbh->prepare('INSERT INTO tbl_server_infos (public_key) VALUES (?)');
            $sth->execute(array($_SESSION['server_key']));
        }

		if (!isset($_SESSION['logged']) || !isset($_SESSION['user_id']) || !$_SESSION['logged'] ||
            isset($_POST['username']) || isset($_POST['passwd'])) {
			$_SESSION['logged'] = false;
			
			if (isset($_POST['username']) && isset($_POST['password'])) {
				$sth = $dbh->prepare('SELECT * FROM tbl_user WHERE username = ? AND password = ?');
				$sth->execute(array($_POST['username'], md5($_POST['password'])));
			
				$res = $sth->fetchAll();                
				if (count($res) == 1) {
					$_SESSION['logged'] = true;
					$_SESSION['user_id'] = $res[0]['id'];
                    $_SESSION['user_mode'] = $res[0]['type'];
                    $_SESSION['user_used'] = $res[0]['used'];
				}
			}
		}
        
        if (isset($_SESSION['logged']) && $_SESSION['logged']) {
            $sth = $dbh->prepare('SELECT b.id FROM tbl_usergroup a JOIN tbl_group b ON a.id_group = b.id WHERE a.id_user = ?');
            $sth->execute(array($_SESSION['user_id']));
            $res = $sth->fetchAll(PDO::FETCH_ASSOC);
            $_SESSION['user_groups'] = array();
            foreach ($res as $line)
                array_push($_SESSION['user_groups'], $line['id']);
        }
	}
	catch (PDOException $e) {
	    die(json_encode(array("error"=>$e->getMessage())));
	}
	
?>