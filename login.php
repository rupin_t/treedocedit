<?php
    include ('core.php');
	
	if ($_SESSION['logged'])
        print json_encode(array("logged"=>1, "id"=>$_SESSION['user_id'], "mode"=>$_SESSION['user_mode'],
        						"used"=>!!$_SESSION['user_used'], "server_key"=>$_SESSION['server_key']));
	else
		print json_encode(array("logged"=>0, "error"=>"Wrong username or password.", "errno"=>5));
?>