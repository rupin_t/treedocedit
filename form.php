<?php
    include('core.php');
	
	$response = '{"error":"nothing done", "errno":-1}';
	if ($_SESSION['logged']) {
		$jquery = file_get_contents('libs/jquery/js/jquery-1.7.2.min.js') .
					file_get_contents('libs/jquery/js/jquery.form.js').
					file_get_contents('libs/jquery/js/jquery.json-2.3.min.js');
		$script = file_get_contents('js/form.js');
		$html = "<!DOCTYPE html><head><script>$jquery $script</script></head><body>";
		$response = json_encode(array('data'=>array($html, '</body></html>')));
	}
	print $response;
?>