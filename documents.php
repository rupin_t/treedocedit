<?php
    
    include('core.php');

	$response = '{"error":"nothing done", "errno":-1}';
    if ($_SESSION['logged']) {

		if (isset($_POST['action'])) {
			if ($_POST['action'] == 'insert' && isset($_POST['name']) && isset($_POST['json'])) {
				$sth = $dbh->prepare('INSERT INTO tbl_documents (name, json, owner_id) VALUES (?, ?, ?)');
				if ($sth->execute(array($_POST['name'], $_POST['json'], $_SESSION['user_id']))) {
					$sth = $dbh->prepare('SELECT id FROM tbl_documents WHERE owner_id = ? ORDER BY id DESC LIMIT 1');
					$sth->execute(array($_SESSION['user_id']));
					$res = $sth->fetchAll();
					$response = '{"msg":"Document inserted.", "id":'.$res[0]['id'].'}';
				}
			}
			else if ($_POST['action'] == 'delete' && isset($_POST['id'])) {
				$sth = $dbh->prepare('DELETE FROM tbl_documents WHERE id = ?');
				$sth->execute(array($_POST['id']));
				$response = '{"msg":"Document deleted."}';
			}
			else if ($_POST['action'] == 'update' && isset($_POST['id']) && isset($_POST['data']) && isset($_POST['what'])) {
				$sth = $dbh->prepare('SELECT version FROM tbl_documents WHERE owner_id = ? AND id = ?');
				$sth->execute(array($_SESSION['user_id'], $_POST['id']));
				$d = $sth->fetchAll(PDO::FETCH_ASSOC);
				if (count($d) == 1) {
					if ($_POST['what'] == 'json' || $_POST['what'] == 'name') {
		    			$sth = $dbh->prepare('UPDATE tbl_documents SET ' . $_POST['what'] . ' = ?, version = ? WHERE owner_id = ? AND id = ?');
						$sth->execute(array($_POST['data'], $d[0]['version'] + 1, $_SESSION['user_id'], $_POST['id']));
						$response = '{"msg":"Document has been updated."}';
					}
				}
			}
    	}
		else if (isset($_GET['query'])) {
	    	if ($_GET['query'] == 'list') {
                $q = 'SELECT id, name, version FROM tbl_documents WHERE owner_id = ?';
				$sth = $dbh->prepare($q);
				$sth->execute(array($_SESSION['user_id']));
                $d = $sth->fetchAll(PDO::FETCH_ASSOC);
                foreach ($d as &$line) {
                    $line['group_id'] = -1;
                    $line['group_name'] = 'mine';
                }
                if (isset($_GET['withGroups'])) {
                    $qMarks = str_repeat('?,', count($_SESSION['user_groups']) - 1) . '?';
                    $sth = $dbh->prepare("
                        SELECT a.id, a.name, a.version, c.id AS 'group_id', c.name AS  'group_name'
                        FROM tbl_documents a
                        JOIN tbl_docgroup b ON a.id = b.id_document
                        JOIN tbl_group c ON c.id = b.id_group
                        WHERE b.id_group
                        IN ( $qMarks )
                        ORDER BY b.id_group ASC
                    ");
                    $sth->execute($_SESSION['user_groups']);
                    $d = array_merge($d, $sth->fetchAll(PDO::FETCH_ASSOC));
                }
				$response = json_encode(array('data'=>$d));
	    	}
			else if ($_GET['query'] == 'fromId' && isset($_GET['id'])) {
                $qMarks = str_repeat('?,', count($_SESSION['user_groups']) - 1) . '?';
				$sth = $dbh->prepare("
                    SELECT a.json, a.name, a.version, c.name AS group_name, c.id AS group_id
                    FROM tbl_documents a
                    LEFT JOIN tbl_docgroup b ON a.id = b.id_document
                    LEFT JOIN tbl_group c ON c.id = b.id_group
                    WHERE a.id = ? AND c.id IN ( $qMarks )
                ");
				$sth->execute(array_merge(array($_GET['id']), $_SESSION['user_groups']));
				$d = $sth->fetchAll(PDO::FETCH_ASSOC);
                foreach ($d as &$line) {
                    if ($line['group_id'] == null) $line['group_id'] = -1;
                    if ($line['group_name'] == null) $line['group_name'] = 'mine';
                }
				if (count($d) == 1)
                    $response = json_encode(array("data"=>json_decode($d[0]['json']), "name"=>$d[0]['name'], "version"=>$d[0]['version'], "group_id"=>$d[0]['group_id'], "group_name"=>$d[0]['group_name']));
				else $response = '{"error":"unavailable document", "errno":1}';
			}
		}
    }
	else $response = '{"error":"not logged", "errno":"0"}';

	print $response;
?>