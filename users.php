<?php
    include('core.php');
	
	$response = '{"error":"nothing done", "errno":-1}';
	if ($_SESSION['logged']) {
        if (isset($_GET['query'])) {
    		if ($_GET['query'] == 'list') {
	    		$sth = $dbh->prepare('SELECT id, username, type, used FROM tbl_user');
		    	$sth->execute(array());
			    $response = json_encode(array('data'=>$sth->fetchAll(PDO::FETCH_ASSOC)));
		    }
        }
        if (isset($_POST['action'])) {
            if ($_POST['action'] == 'redefinePassword' && isset($_POST['new'])) {
                $sth = $dbh->prepare('UPDATE tbl_user SET password = ?, used = 1 WHERE id = ?');
                $_SESSION['user_used'] = true;
                $res = $sth->execute(array(md5($_POST['new']), $_SESSION['user_id']));
                if ($res)
                    $response = json_encode(array('msg'=>'Your password has been redefined.'));
            }
        }
	}
	print $response;
?>