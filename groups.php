<?php
    include('core.php');
    
	$response = '{"error":"nothing done", "errno":-1}';
    
    if ($_GET['query'] == 'fromId' && isset($_GET['id'])) {
        $sth = $dbh->prepare('SELECT * FROM tbl_group WHERE id = ?');
        $sth->execute(array($_GET['id']));
        $d = $sth->fetchAll(PDO::FETCH_ASSOC);
        if (count($d) == 1) {
            $response = json_encode(array('data'=>$d[0]));
        }
    }
    else if ($_GET['query'] == 'list') {
        $sth = $dbh->prepare('SELECT * FROM tbl_group');
        $sth->execute(array());
        $d = $sth->fetchAll(PDO::FETCH_ASSOC);
        $response = json_encode(array('data'=>$d));
    }
    else if ($_GET['query'] == 'listByGroup' &&
            ($_GET['what'] == 'users' || $_GET['what'] == 'documents') &&
            $_SESSION['user_mode'] == 0) {

        if ($_GET['what'] == 'users') {
            $sth = $dbh->prepare('
                SELECT a.id as "id_user", a.username, c.id as "id_group"
                FROM tbl_user a
                JOIN tbl_usergroup b ON a.id = b.id_user
                JOIN tbl_group c ON b.id_group = c.id
            ');
        }
        else if ($_GET['what'] == 'documents') {
            $sth = $dbh->prepare('
                SELECT *
                FROM tbl_documents a
                JOIN tbl_docgroup b ON a.id = b.id_document
                JOIN tbl_group c ON b.id_group = c.id
            ');
        }

        $sth->execute(array());
        $res = $sth->fetchAll(PDO::FETCH_ASSOC);
        $d = array();
        foreach ($res as $key => $value) {
            if (!isset($d[$value['id_group']]))
                $d[$value['id_group']] = array();
            array_push($d[$value['id_group']], $value);
        }

        $response = json_encode($d);
    }

    print $response;
?>