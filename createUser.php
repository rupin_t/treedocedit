<?php

    try {
    	$dbh = new PDO('mysql:host=localhost;port=8889;dbname=treedoc', 'root', 'root');

		if (!isset($_SESSION['logged']) || !isset($_SESSION['user_id']) || !$_SESSION['logged']) {
			$_SESSION['logged'] = false;
			
			if (isset($_POST['username']) && isset($_POST['password'])) {
				$sth = $dbh->prepare('SELECT * FROM tbl_user WHERE username = ?');
				$sth->execute(array($_POST['username']));

				$res = $sth->fetchAll();
				if (count($res) == 1) {
                    print '{"error":"This username is already taken, "errno":-1}';
                    die();
				}

                $q = 'INSERT INTO tbl_user (username, password';
                if (isset($_POST['mode'])) $q .= ', type';
                $q .= ') VALUES (?, ?';
                if (isset($_POST['mode'])) $q .= ', ?';
                $q .= ')';
                
                $sth = $dbh->prepare($q);
                $args = array($_POST['username'], md5($_POST['password']));
                if (isset($_POST['mode']))
                    array_push($args, $_POST['mode']);
                $sth->execute($args);
                
                $sth = $dbh->prepare('SELECT id FROM tbl_user WHERE username = ? AND password = ?');
                $sth->execute(array($_POST['username'], md5($_POST['password'])));
                
                $res = $sth->fetchAll();
                if (count($res) == 1) {
                    print '{"id":'.$res[0]['id'].', "msg":"Your profile has been created."}';
                }
                else {
                    print '{"error":"Internal error, your profile hasn\'t been created.", "errno":-1}';
                }
			}
		}
	}
	catch (PDOException $e) {
	    print '{"error":"' . $e->getMessage() . '","errno":-1}';
	    die();
	}

?>