/**
 * @author rupin_t
 */

var json;

$(function() {
	$('form').submit(function() {
		
		json = {}
        jQuery.map($(this).serializeArray(), function(n, i) {
            json[n['name']] = n['value'];
        });
        json = $.toJSON(json);
        
		return true;
	});
});
