var documents = [];
var selectedDocument = 0;

var data = [{name: 'root', root: true}];
var selectedNode = 0;
var maxIndex = 0;

$(function() {
    
    $.ajax({
        url: 'documents.php?query=list',
        async: false,
        dataType: 'JSON',
        success: function(d) {
        	if (d['errno'] !== undefined)
        		alert(d['error']);
        	else
	        	documents = d['data'];
        },
        error: function(jqXHR, textStatus, errorThrown) {
            alert(errorThrown);
        }
    });
    
    $('#list-anchor').draggable({
    	handler: 'div.ui-widget-handler',
    	stack: '.focus'
    }).resizable();
    
    $('#list-content').tree({
        data: documents,
        selectable: true,
        onCreateLi: function(node, $li) {
            $li.addClass('ui-widget-content');
        }
    }).bind(
        'tree.click',
        function(event) {
            loadDocument(event.node);
        }
    );
    
    $('.focus').click(function(event) {
    	if ($(event.target).is('.focus')) {
	    	var max = 0;
	    	for (var i = 0, a = $('div.focus'); i < a.length; ++i) {
	    		var o = parseInt($(a[i]).css('z-index'));
	    		if (o !== undefined && max < o) max = o;
	    	}
	    	$(this).css('z-index', max+1);
	    }
    });
    
	$('#add-document-button').button().click(function() {
		$('body').append('<div id="create-document-window"><p>Name <input id="document-name" type="text"/></p></div>');
		$('#create-document-window').dialog({
			create: function() {
				$('#document-name').button().addClass('my-textfield');
			},
			buttons: {'Create': function() {
				var n = $('#document-name').val();
				$.ajax({
		    	    url: 'documents.php',
			        dataType: 'JSON',
			        data: {action: 'insert', name: n, json: $.toJSON(data)},
			        type: 'POST',
			        success: function(d) {
			        	if (d['errno'] !== undefined)
			        		alert(d['error']);
			        	else {
			        		$('#list-content').tree('appendNode', {
			        			name: n,
			        			id: d['id']
			        		});
			        	}
			        	$('#create-document-window').dialog('close');
			        },
			        error: function(jqXHR, textStatus, errorThrown) {
			            alert(errorThrown);
			            $('#create-document-window').dialog('close');
			        }			        
			    });
			}},
			close: function() {
				$('#create-document-window').remove();
			}
		});
		
	});
	
	$('#delete-document-button').button().click(function() {
		if (selectedDocument !== undefined && selectedDocument) {
			$.ajax({
				url: 'documents.php',
				dataType: 'JSON',
				data: {action: 'delete', id: selectedDocument.id},
				type: 'POST',
				success: function(d) {
					if (d['errno'] !== undefined)
						alert(d['error']);
					else {
						$('#list-content').tree('removeNode', selectedDocument);
						$('#tree-view-anchor, #properties-anchor').hide();
					}
				},
				error: function(jqXHR, textStatus, errorThrown) {
					alert(errorThrown);
				}
			});
		}
		else
			alert("You must select a document to delete.");
	});

	// document views
    $('#label-name').change(function() {
        var $this = $(this);
        selectedNode.name = $this.val();
        $(selectedNode.element).find('span.title:first').text($this.val());
    });

    $('#content-html').button().click(function() {
        $('#content-html-dialog').remove();
        $('body').append('<div id="content-html-dialog"><div id="hidden-content" hidden></div><textarea></textarea></div>');
        $('#content-html-dialog').dialog({
            'resizable': false,
            'width': $(document).width() * 0.8,
            'height': $(document).height() * 0.8,
            'title': 'Html content',
            'modal': true,
            'buttons': { 'Ok':function() {
                selectedNode.content = $('#content-html-dialog').find('textarea').val();
                selectedNode.labels = [];
                
                $('#hidden-content').text($('#content-html-dialog').find('textarea').val());
                var inputs = $('#hidden-content').find('input');
                for (var x = 0; x < inputs.length; ++x) {
                    selectedNode.labels.push(inputs[x].name);
                }
                $(this).dialog('close');
            }}
        }).find('textarea')
          .width($(document).width() * 0.75)
          .height($(document).height() * 0.6)
          .val(selectedNode.content);
    });
    // height: $(document).height() * 0.6

    $('#img-button').button().click(function() {
        $('#select-image').click();
    });

	$('#delete-label-button').button().click(function() {
        if (!selectedNode || selectedNode === undefined)
            alert('You must select a node before');
        else if (selectedNode.root !== undefined)
            alert('You cannot remove the root');
        else {
            $('#tree-content').tree('removeNode', selectedNode);
            $('#properties-anchor').hide();
            refreshNode(0);
        }
        return false;
    });

    $('#save-button').button().click(function() {
        var saved = $.evalJSON($('#tree-content').tree('toJson'))[0];
        if (!formatDataBeforeSaving(saved, 0)) {
            alert('Cannot save, some properties are missing');
            return;
        }
        $.ajax({
            async: false,
            type: 'POST',
            url: 'documents.php',
            data: {action: 'update', what: 'json', id: selectedDocument.id, data: $.toJSON(saved)},
            dataType: 'JSON',
            success: function(res) {
            	if (res['errno'] !== undefined)
	            	alert(res['error']);
	            else alert(res['msg']);
            },
            error: function(jqXHR, textStatus, errorThrown) {
    		    alert(errorThrown);
	        }
        });
    });

    $('#add-label-button').button().click(function() {
        if (!selectedNode || selectedNode === undefined)
            alert('You must select a node before');
        else {
            $('#tree-content').tree('appendNode',
                {name: 'newNode-' + maxIndex++},
                selectedNode
            );
            refreshNode(selectedNode);
        }
        return false;
    });

    $('#size-x').change(function() {
        selectedNode.size[0] = parseInt($(this).val());
    });

    $('#size-y').change(function() {
        selectedNode.size[1] = parseInt($(this).val());
    });
  
    $('#show-ratio').change(function() {
        selectedNode.scale = parseFloat($(this).val());
    });
  
    $('#tree-content').tree({
        data: data,
        dragAndDrop: true,
        selectable: true,
        onCreateLi: function(node, $li) {
            $li.addClass('ui-widget-content');
        },
        onCanMove: function(node) {
            return node.root === undefined;
        },
        onCanMoveTo: function(moved_node, target_node, position) {
            return !(target_node.root !== undefined && position != 'inside');
        }
    }).bind(
        'tree.click',
        function(event) {
            refreshNode(event.node);
            $('#properties-anchor').show();
        }
    );

    function loadDocument(doc) {
    	if (doc === undefined || selectedDocument.id == doc.id)
    		return;
    	selectedDocument = doc;

	    $.ajax({
	        url: 'documents.php?query=fromId&id=' + doc.id,
	        async: false,
	        dataType: 'JSON',
	        success: function(d) {
	        	if (d['errno'] !== undefined)
        			alert(d['error']);
	        	else {
		            data = [d['data']];
		            data[0].root = true;
		      	}
	        },
	        error: function(jqXHR, textStatus, errorThrown) {
	            alert(errorThrown);
	        }
	    });
	    
	    $('#full-image').parent().empty().append('<img id="full-image"/>');
	    $('#label-anchor-containor').empty();
	    
	    // first page
	    $('#tree-view-anchor').draggable({
	        handle: "div.ui-widget-header",
	        stack: '.focus'
	    }).resizable().show();
	
	    $('#label-type-select').selectable({'selected': function() {
	        selectedNode.type = $('#label-type-select > .ui-selected').text();
	    }});

	    // second page
	    $('#properties-anchor').draggable({
	        handle: "div.ui-widget-header",
	        stack: '.focus'
	    }).resizable().hide();
	    $('#properties-anchor input').button().addClass('my-textfield');
	    
	    $('#tree-content').tree('loadData', data);
	}
    
    function readImage(evt) {
        var files = evt.target.files;
        for (var i = 0, f; f = files[i]; i++) {
            var reader = new FileReader();
            reader.onloadend = function(evt) {
                if (evt.target.readyState == FileReader.DONE) {
                    var b64 = $.base64.encode(evt.target.result);
                    selectedNode.fullImage = b64;
                }
                refreshNode(selectedNode);
            };
            reader.readAsBinaryString(f);
        }
    }

    function refreshNode(node) {
        selectedNode = node;
        var $contain = $('#label-anchor-containor');

        $contain.empty();
        if (node !== 0) {
            if (node.fullImage === undefined)
            	$('#full-image').parent().empty().append('<img id="full-image"/>');
            else {
                $('#full-image').attr('src', 'data:image;base64,' + node.fullImage);
                $('#full-image').height($(window).height() * 0.95);
            }

            if (node.root) {
                $('#anchor-position').hide();
                $('#content-html').hide();
                $('#size-x').parent().hide();
                $('#label-type-select').hide();
                $('#show-ratio').parent().hide();
            }
            else {
                if (node.position === undefined)
                    node.position = [0, 0];
                $('#anchor-position').text('Position ' + Math.round(node.position[0]) + ', ' + Math.round(node.position[1])).show();
                $('#content-html').show();
                $('#size-x').parent().show();
                $('#label-type-select').show();
                $('#show-ratio').parent().show();
            }

            $('#label-name').val(node.name);
            initSelectImg();

            if (node.type === undefined)
                node.type = $('#label-type-select > .ui-selected').text();
            else {
                $('#label-type-select > .ui-selected').removeClass('ui-selected');
                $('#label-type-select > li').each(function() {
                    if ($(this).text() == node.type) {
                        $(this).addClass('ui-selected');
                        return false;
                    }
                });
            }

            if (node.size === undefined)
                node.size = [25, 25];
            $('#size-x').val(node.size[0]);
            $('#size-y').val(node.size[1]);

            if (node.scale === undefined)
                node.scale = 1.0;
            $('#show-ratio').val(node.scale);

            for (var s in node.children) {
                s = node.children[s];

                if (s.rposition === undefined)
                    s.rposition = [0, 0];
                $contain.append('<p id="anchor-'+ s.name +'" class="anchor" ' +
                                'style="position: absolute; left: '+ s.rposition[0] +
                                'px; top: '+ s.rposition[1] +'px;">X</p>');

                $('#label-anchor-containor > .anchor:last').draggable({
                    'cursor': 'crosshair',
                    'start': function(event, ui) {
                        ui.helper.click();
                    },
                    'stop': function(event, ui) {
                        var l = getNodeData(ui.helper[0].id);
                        l.rposition = [ui.offset.left, ui.offset.top];
                        l.position = [(ui.offset.left - ui.helper.width() / 2) * 100 / ($('#full-image').width() + 1),
                                        (ui.offset.top + ui.helper.height() / 2) * 100 / ($('#full-image').height() + 1)];
                    }
                }).click(function() {
                    $('.selected-label').removeClass('selected-label');
                        var $this = $(this);
                        $this.addClass('selected-label');
                        var l = getNodeData($this[0].id);
                        $(l.element).find('span:first').addClass('selected-label');
                });
            }
        }
    }

    function getNodeData(id) {
        for (var l in selectedNode.children) {
            l = selectedNode.children[l];
            if ('anchor-' + l.name == id)
                return l;
        }
    }

    function initSelectImg() {
        if (selectedNode.imagePath !== undefined)
            $('#image-name').text('Image ' + selectedNode.imagePath).show();
        else
            $('#image-name').hide();
        
        $('#select-image').remove();
        $('#properties-anchor > div.toolbar').append('<input id="select-image" type="file"/>');
        $('#select-image')[0].addEventListener('change', readImage, false);
        $('#select-image').hide().change(function() {
            selectedNode.imagePath = $(this).val();
        }).button().addClass('my-textfield');
    }

    function formatDataBeforeSaving(node, lvl, path) {
        if (path === undefined)
            path = '0';
        node.path = path;
        if (node.hash === undefined)
            node.hash = path + '-' + new Date().getTime();
        console.log(node.hash);

        if (lvl > 0 && node.type === undefined) {
            return false;
        }
        if (node.fullImage === undefined && (node.type == "transition" || lvl === 0)) {
            return false;
        }
        if (node.name === undefined) {
            return false;
        }
        
        if (node.position === undefined)
            node.position = [0, 0];
        if (node.size === undefined)
            node.size = [25, 25];
        if (node.scale === undefined)
            node.scale = 1.0;

        for (var child in node.children)
            if (!formatDataBeforeSaving(node.children[child], lvl+1, path + '.' + child))
                return false;        
        return true;
    }

});